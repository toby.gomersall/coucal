import binascii
import os

from sys import argv

# This script converts a binary file to a .coe file which can be added to a
# Xilinx Vivado block RAM.
#
# To run this program:
#
#     python binary_to_coe.py <binary_input_filename> <coe_output_filename>

def reverse_endian_16(input_int):
    ''' Takes a 16 bit `input_int` and reverses the byte order.

    0x1234 => 0x3412
    '''

    result = (
        ((input_int <<  8) & 0xFF00) |
        ((input_int >>  8) & 0x00FF)
    )

    return result

def reverse_endian_32(input_int):
    ''' Takes a 32 bit `input_int` and reverses the byte order.

    0x12345678 => 0x78563412
    '''

    result = (
        ((input_int << 24) & 0xFF000000) |
        ((input_int <<  8) & 0x00FF0000) |
        ((input_int >>  8) & 0x0000FF00) |
        ((input_int >> 24) & 0x000000FF)
    )

    return result

def convert(binary_input_file, coe_output_file):
    ''' Converts a binary input file into a .coe output file.
    '''

    # Specify the number of bits in each line of the output coe file.
    # Calculate how many hex characters this equates to.
    bits_per_line = 32
    hex_char_bit_length = 4
    n_hex_chars_per_line = bits_per_line//hex_char_bit_length

    with open(binary_input_file, 'rb') as f:
        # Read all values in from the binary file and convert them to hex
        hex_data = binascii.hexlify(f.read())

    # Separate the hex_data into strings of the correct length, convert to
    # integer and then switch the endianness. This is important to maintain
    # the endianness of the input data when the output coe file is added to
    # a Xilinx ROM in Vivado.
    if len(hex_data) % 8 == 0:
        # The input binary is a multiple of 4 bytes so can be broken up nicely
        # into 8 characters per line.
        int_list = [
            reverse_endian_32(int(hex_data[n:n+n_hex_chars_per_line], 16))
            for n in range(0, len(hex_data), n_hex_chars_per_line)]

    elif len(hex_data) % 4 == 0:
        # The input binary has 2 bytes at the end which need to be byte
        # reversed. So we break up most of the input into 8 characters per
        # line but leave the last 4 characters.
        int_list = [
            reverse_endian_32(int(hex_data[n:n+n_hex_chars_per_line], 16))
            for n in range(0, len(hex_data)-4, n_hex_chars_per_line)]

        # Byte reverse the last 4 characters.
        int_list.append(reverse_endian_16(int(hex_data[-4:], 16)))

    else:
        # Check that binary_input_file is a valid length. Base integer
        # instructions are 4 bytes and compressed instructions are 2 bytes so
        # the length of the hex data should be a multiple of 2 bytes (4 hex
        # characters) or 4 bytes (8 hex characters).
        raise ValueError(
            "Bin to coe conversion failed: The binary is an invalid length. "
            "RISC-V base integer instructions are 4 bytes and compressed "
            "instructions are 2 bytes so the binary should be a multiple of "
            "2 or 4 bytes.")

    with open(coe_output_file, 'w') as f:

        # Write a header to the output file
        f.write(
            '; This file is a .coe file that was converted by the\n'
            '; binary_to_coe.py. Please use to initialise a xilinx ROM.\n')

        # Specify that the radix is hex (16)
        f.write('memory_initialization_radix = 16;\n')

        # Create the memory_initialization_vector, which will be populated
        # with hex_data below
        f.write('memory_initialization_vector =\n')

        for n, output_val in enumerate(int_list):
            # Convert each value in the int_list to hex, pad it with zeroes
            # and write to the coe_output_file
            f.write(str.format('{:08X}', output_val))

            if n < len(int_list) - 1:
                # End all but the last line with a comma
                f.write(',\n')
            else:
                # End the last line with a semi colon
                f.write(';')

if __name__=='__main__':

    # Read the input and output file names
    binary_input_file = argv[1]
    coe_output_file = argv[2]

    convert(binary_input_file, coe_output_file)
