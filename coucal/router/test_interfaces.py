import random

from math import log

from kea.test_utils import KeaTestCase as TestCase

from coucal.test_utils import random_string_generator
from coucal.core import ADDR_BITWIDTH, DATA_BITWIDTH, W_STROBE_BITWIDTH

from .interfaces import PeripheralsInterface, PeripheralInterface

from .test_utils import random_address_map_generator

DATA_BYTEWIDTH = int(DATA_BITWIDTH/8)

class TestInterfaces(TestCase):

    def setUp(self):

        # Generate a random address map
        self.n_peripherals = random.randrange(2, 10)
        self.address_map = random_address_map_generator(self.n_peripherals)

    def test_constants(self):
        ''' The interface should use an address bitwidth of 32, a data
        bitwidth of 32 and a write strobe bitwidth of 4.
        '''

        assert(ADDR_BITWIDTH == 32)
        assert(DATA_BITWIDTH == 32)
        assert(W_STROBE_BITWIDTH == 4)

    def test_empty_address_map(self):
        ''' The `PeripheralsInterface` should raise an error if the
        `address_map` is empty.
        '''

        invalid_address_map = {}

        self.assertRaisesRegex(
            ValueError,
            ('PeripheralsInterface: address_map should contain at least one '
             'entry.'),
            PeripheralsInterface,
            invalid_address_map
        )

    def test_n_bytes_less_than_data_bytewidth(self):
        ''' The `PeripheralsInterface` should raise an error if the
        `address_map` contains an `n_bytes` which is less than the bytewidth
        of the data signals.
        '''

        # Select a random peripheral
        peripheral = random.choice(list(self.address_map.keys()))

        # generate a random and invalid n_bytes
        n_bytes = random.randrange(0, DATA_BYTEWIDTH)

        self.address_map[peripheral]['n_bytes'] = n_bytes

        self.assertRaisesRegex(
            ValueError,
            ('PeripheralsInterface: All n_bytes should be greater '
             'than the data byte width ' + hex(DATA_BYTEWIDTH) +
             '. ' + peripheral + ' has an n_bytes of ' +
             hex(n_bytes) + '.'),
            PeripheralsInterface,
            self.address_map
        )

    def test_negative_n_bytes(self):
        ''' The `PeripheralsInterface` should raise an error if the
        `address_map` contains a negative `n_bytes`.
        '''

        # Select a random peripheral
        peripheral = random.choice(list(self.address_map.keys()))

        # Generate a random, negative n_bytes
        n_bytes = random.randrange(-1000, 0)

        self.address_map[peripheral]['n_bytes'] = n_bytes

        self.assertRaisesRegex(
            ValueError,
            ('PeripheralsInterface: All n_bytes should be greater '
             'than the data byte width ' + hex(DATA_BYTEWIDTH) +
             '. ' + peripheral + ' has an n_bytes of ' +
             hex(n_bytes) + '.'),
            PeripheralsInterface,
            self.address_map
        )

    def test_n_bytes_not_a_multiple_of_data_bytewidth(self):
        ''' The `PeripheralsInterface` should raise an error if the
        `address_map` contains an `n_bytes` which is not a multiple of the
        byte width of the data signals.
        '''

        # Select a random peripheral
        peripheral = random.choice(list(self.address_map.keys()))

        # Generate a random n_bytes
        n_bytes = random.randrange(DATA_BYTEWIDTH, 2**ADDR_BITWIDTH)

        if n_bytes % DATA_BYTEWIDTH == 0:
            # If n_bytes is a multiple of DATA_BYTEWIDTH then add one to make
            # it invalid
            n_bytes += 1

        self.address_map[peripheral]['n_bytes'] = n_bytes

        self.assertRaisesRegex(
            ValueError,
            ('PeripheralsInterface: All n_bytes should be a '
             'multiple of the data byte width ' + hex(DATA_BYTEWIDTH) +
             '. ' + peripheral + ' has an n_bytes of ' +
             hex(n_bytes) + ' which is not a multiple of ' +
             hex(DATA_BYTEWIDTH) + '.'),
            PeripheralsInterface,
            self.address_map
        )

    def test_n_bytes_not_a_power_of_two(self):
        ''' The `PeripheralsInterface` should raise an error if the
        `address_map` contains an `n_bytes` which is not a power of 2.
        '''

        # Select a random peripheral
        peripheral = random.choice(list(self.address_map.keys()))

        # Generate a random n_bytes
        n_bytes = random.randrange(
            DATA_BYTEWIDTH, 2**ADDR_BITWIDTH, DATA_BYTEWIDTH)

        if (n_bytes & (n_bytes-1)) != 0:
            # If n_bytes is a power of 2 then add 2 data_bytewidths to make it
            # invalid
            n_bytes += 2*DATA_BYTEWIDTH

        self.address_map[peripheral]['n_bytes'] = n_bytes

        self.assertRaisesRegex(
            ValueError,
            ('PeripheralsInterface: All n_bytes should be a power of '
             '2. This rule means all peripherals have a unique '
             'routing address. ' + peripheral + ' has an n_bytes of ' +
             hex(n_bytes) + ' which is not a power of 2.'),
            PeripheralsInterface,
            self.address_map
        )

    def test_negative_offset(self):
        ''' The `PeripheralsInterface` should raise an error if the
        `address_map` contains an offset which is less than 0.
        '''

        # Select a random peripheral
        peripheral = random.choice(list(self.address_map.keys()))

        # Generate a random negative offset
        offset = random.randrange(-100, 0)

        self.address_map[peripheral]['offset'] = offset
        self.address_map[peripheral]['n_bytes'] = DATA_BYTEWIDTH

        self.assertRaisesRegex(
            ValueError,
            ('PeripheralsInterface: All offsets should be greater than or '
             'equal to 0. ' + peripheral + ' has an offset of ' +
             hex(offset) + '.'),
            PeripheralsInterface,
            self.address_map
        )

    def test_just_invalid_offset(self):
        ''' The `PeripheralsInterface` should raise an error if the
        `address_map` contains an offset which equals the first invalid
        address.
        '''

        # Select a random peripheral
        peripheral = random.choice(list(self.address_map.keys()))

        # Set the offset to be just invalid
        offset = 2**ADDR_BITWIDTH

        self.address_map[peripheral]['offset'] = offset
        self.address_map[peripheral]['n_bytes'] = DATA_BYTEWIDTH

        self.assertRaisesRegex(
            ValueError,
            ('PeripheralsInterface: All offsets should be less than ' +
             hex(2**ADDR_BITWIDTH) + '. ' + peripheral + ' has an '
             'offset of ' + hex(offset) + '.'),
            PeripheralsInterface,
            self.address_map
        )

    def test_invalid_offset(self):
        ''' The `PeripheralsInterface` should raise an error if the
        `address_map` contains an offset which exceeds the address space.
        '''

        # Select a random peripheral
        peripheral = random.choice(list(self.address_map.keys()))

        # Generate a random, invalid offset
        offset = (
            random.randrange(
                2**ADDR_BITWIDTH+DATA_BYTEWIDTH,
                2**ADDR_BITWIDTH+1000,
                DATA_BYTEWIDTH))

        self.address_map[peripheral]['offset'] = offset
        self.address_map[peripheral]['n_bytes'] = DATA_BYTEWIDTH

        self.assertRaisesRegex(
            ValueError,
            ('PeripheralsInterface: All offsets should be less than ' +
             hex(2**ADDR_BITWIDTH) + '. ' + peripheral + ' has an '
             'offset of ' + hex(offset) + '.'),
            PeripheralsInterface,
            self.address_map
        )

    def test_offset_not_a_multiple_of_n_bytes(self):
        ''' The `PeripheralsInterface` should raise an error if the
        `address_map` contains an offset which is not a multiple of `n_bytes`.
        '''

        # Select a random peripheral
        peripheral = random.choice(list(self.address_map.keys()))

        # Generate a random, invalid offset
        offset = self.address_map[peripheral]['offset'] + 1

        self.address_map[peripheral]['offset'] = offset
        self.address_map[peripheral]['n_bytes'] = DATA_BYTEWIDTH

        self.assertRaisesRegex(
            ValueError,
            ('PeripheralsInterface: All peripheral offsets should be '
             'a multiple of the n bytes assigned to the peripheral. '
             'This rule combined with all n bytes being a power of '
             'two means that the peripheral can use n bits for '
             'indexing in to its memory space and the router can use '
             'the remaining bits for routing. n is calculated by '
             'taking the log base 2 of n_bytes. ' + peripheral +
             ' has an invalid offset.'),
            PeripheralsInterface,
            self.address_map
        )

    def test_random_overlaps(self):
        ''' The `PeripheralsInterface` should raise an error if the
        `address_map` contains overlapping memory regions.
        '''

        # Extract the peripherals
        peripherals = list(self.address_map.keys())

        # Select two random memory regions which will overlap
        peripheral_0, peripheral_1 = random.sample(peripherals, 2)

        # Extract the upper and lower addresses in the peripheral 0 memory
        # region
        peripheral_0_lower = self.address_map[peripheral_0]['offset']
        peripheral_0_upper = (
            peripheral_0_lower + self.address_map[peripheral_0]['n_bytes'])

        # Choose a random offset in the memory region of peripheral 0
        self.address_map[peripheral_1]['offset'] = (
            random.randrange(
                peripheral_0_lower, peripheral_0_upper, DATA_BYTEWIDTH))
        self.address_map[peripheral_1]['n_bytes'] = DATA_BYTEWIDTH

        # Get the order that the DUT reports the peripherals correct so we can
        # match the error message.
        if peripherals.index(peripheral_0) < peripherals.index(peripheral_1):
            error_0 = peripheral_0
            error_1 = peripheral_1

        else:
            error_0 = peripheral_1
            error_1 = peripheral_0

        self.assertRaisesRegex(
            ValueError,
            ('PeripheralsInterface: Peripheral addresses should not '
             'overlap. ' + error_0 + ' and '
             + error_1 + ' are overlapping.'),
            PeripheralsInterface,
            self.address_map
        )

    def test_just_overlapping(self):
        ''' The `PeripheralsInterface` should raise an error if the
        `address_map` contains memory regions that overlap even if the overlap
        is very small.
        '''

        # Extract the peripherals
        peripherals = list(self.address_map.keys())

        # Select two random memory regions which will overlap
        peripheral_0, peripheral_1 = random.sample(peripherals, 2)

        # Extract the upper and lower addresses in the peripheral 0 memory
        # region
        peripheral_0_lower = self.address_map[peripheral_0]['offset']
        peripheral_0_upper = (
            peripheral_0_lower + self.address_map[peripheral_0]['n_bytes'])

        # Set peripheral 1 offset to the highest valid address of peripheral 0
        self.address_map[peripheral_1]['offset'] = (
            peripheral_0_upper - DATA_BYTEWIDTH)
        self.address_map[peripheral_1]['n_bytes'] = DATA_BYTEWIDTH

        # Get the order that the DUT reports the peripherals correct so we can
        # match the error message.
        if peripherals.index(peripheral_0) < peripherals.index(peripheral_1):
            error_0 = peripheral_0
            error_1 = peripheral_1

        else:
            error_0 = peripheral_1
            error_1 = peripheral_0

        self.assertRaisesRegex(
            ValueError,
            ('PeripheralsInterface: Peripheral addresses should not '
             'overlap. ' + error_0 + ' and '
             + error_1 + ' are overlapping.'),
            PeripheralsInterface,
            self.address_map
        )

    def test_interface_packager_invalid_peripheral(self):
        ''' The `PeripheralsInterface.peripheral_interface_packager` should
        raise an error if the `peripheral` argument is not available on the
        `PeripheralsInterface` interface.
        '''

        # Generate an invalid peripheral name
        address_map_key_length = len(list(self.address_map.keys())[0])
        invalid_peripheral = random_string_generator(address_map_key_length+1)

        router_interface = PeripheralsInterface(self.address_map)

        self.assertRaisesRegex(
            ValueError,
            ('PeripheralsInterface: ' + invalid_peripheral  + ' is not '
             'available on this interface.'),
            router_interface.peripheral_interface_packager,
            invalid_peripheral
        )

    def test_peripheral_interface_invalid_creation_offset(self):
        ''' An instance of the `PeripheralInterface` should be created via the
        `PeripheralsInterface.peripheral_interface_packager`. When it is
        created in that way, the packager will correctly set up the interface.
        If the `PeripheralInterface` is created directly then the attributes
        will not be set up correctly. In this situation the
        `PeripheralInterface.offset` property should raise an error.
        '''

        peripheral_interface = PeripheralInterface()

        def peripheral_interface_offset():
            peripheral_interface.offset

        self.assertRaisesRegex(
            AttributeError,
            ('PeripheralInterface: This interface has not been set up '
             'correctly. PeripheralInterfaces should only be created by '
             'the PeripheralsInterface.peripheral_interface_packager.'),
            peripheral_interface_offset
        )

    def test_peripheral_interface_invalid_creation_n_bytes(self):
        ''' An instance of the `PeripheralInterface` should be created via the
        `PeripheralsInterface.peripheral_interface_packager`. When it is
        created in that way, the packager will correctly set up the interface.
        If the `PeripheralInterface` is created directly then the attributes
        will not be set up correctly. In this situation the
        `PeripheralInterface.n_bytes` property should raise an error.
        '''

        peripheral_interface = PeripheralInterface()

        def peripheral_interface_n_bytes():
            peripheral_interface.n_bytes

        self.assertRaisesRegex(
            AttributeError,
            ('PeripheralInterface: This interface has not been set up '
             'correctly. PeripheralInterfaces should only be created by '
             'the PeripheralsInterface.peripheral_interface_packager.'),
            peripheral_interface_n_bytes
        )

    def test_peripheral_interface_invalid_creation_addressing_bits(self):
        ''' An instance of the `PeripheralInterface` should be created via the
        `PeripheralsInterface.peripheral_interface_packager`. When it is
        created in that way, the packager will correctly set up the interface.
        If the `PeripheralInterface` is created directly then the attributes
        will not be set up correctly. In this situation the
        `PeripheralInterface.n_memory_addressing_bits` property should raise
        an error.
        '''

        peripheral_interface = PeripheralInterface()

        def peripheral_interface_n_memory_addressing_bits():
            peripheral_interface.n_memory_addressing_bits

        self.assertRaisesRegex(
            AttributeError,
            ('PeripheralInterface: This interface has not been set up '
             'correctly. PeripheralInterfaces should only be created by '
             'the PeripheralsInterface.peripheral_interface_packager.'),
            peripheral_interface_n_memory_addressing_bits
        )

    def test_peripheral_interface_invalid_creation_n_routing_bits(self):
        ''' An instance of the `PeripheralInterface` should be created via the
        `PeripheralsInterface.peripheral_interface_packager`. When it is
        created in that way, the packager will correctly set up the interface.
        If the `PeripheralInterface` is created directly then the attributes
        will not be set up correctly. In this situation the
        `PeripheralInterface.n_routing_bits` property should raise an error.
        '''

        peripheral_interface = PeripheralInterface()

        def peripheral_interface_n_routing_bits():
            peripheral_interface.n_routing_bits

        self.assertRaisesRegex(
            AttributeError,
            ('PeripheralInterface: This interface has not been set up '
             'correctly. PeripheralInterfaces should only be created by '
             'the PeripheralsInterface.peripheral_interface_packager.'),
            peripheral_interface_n_routing_bits
        )

    def test_peripheral_interface_invalid_creation_routing_bits_lsb(self):
        ''' An instance of the `PeripheralInterface` should be created via the
        `PeripheralsInterface.peripheral_interface_packager`. When it is
        created in that way, the packager will correctly set up the interface.
        If the `PeripheralInterface` is created directly then the attributes
        will not be set up correctly. In this situation the
        `PeripheralInterface.routing_bits_lsb_index` property should raise an
        error.
        '''

        peripheral_interface = PeripheralInterface()

        def peripheral_interface_routing_bits_lsb_index():
            peripheral_interface.routing_bits_lsb_index

        self.assertRaisesRegex(
            AttributeError,
            ('PeripheralInterface: This interface has not been set up '
             'correctly. PeripheralInterfaces should only be created by '
             'the PeripheralsInterface.peripheral_interface_packager.'),
            peripheral_interface_routing_bits_lsb_index
        )

    def test_peripheral_interface_invalid_creation_data_bitwidth(self):
        ''' An instance of the `PeripheralInterface` should be created via the
        `PeripheralsInterface.peripheral_interface_packager`. When it is
        created in that way, the packager will correctly set up the interface.
        If the `PeripheralInterface` is created directly then the attributes
        will not be set up correctly. In this situation the
        `PeripheralInterface.data_bitwidth` property should raise an error.
        '''

        peripheral_interface = PeripheralInterface()

        def peripheral_interface_data_bitwidth():
            peripheral_interface.data_bitwidth

        self.assertRaisesRegex(
            AttributeError,
            ('PeripheralInterface: This interface has not been set up '
             'correctly. PeripheralInterfaces should only be created by '
             'the PeripheralsInterface.peripheral_interface_packager.'),
            peripheral_interface_data_bitwidth
        )

    def test_peripheral_interface_invalid_creation_data_bytewidth(self):
        ''' An instance of the `PeripheralInterface` should be created via the
        `PeripheralsInterface.peripheral_interface_packager`. When it is
        created in that way, the packager will correctly set up the interface.
        If the `PeripheralInterface` is created directly then the attributes
        will not be set up correctly. In this situation the
        `PeripheralInterface.data_bytewidth` property should raise an error.
        '''

        peripheral_interface = PeripheralInterface()

        def peripheral_interface_data_bytewidth():
            peripheral_interface.data_bytewidth

        self.assertRaisesRegex(
            AttributeError,
            ('PeripheralInterface: This interface has not been set up '
             'correctly. PeripheralInterfaces should only be created by '
             'the PeripheralsInterface.peripheral_interface_packager.'),
            peripheral_interface_data_bytewidth
        )
