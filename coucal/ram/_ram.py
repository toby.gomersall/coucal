from math import log

from myhdl import (
    block, Signal, intbv, always, always_comb, ConcatSignal, enum)

from kea.utils import signal_assigner

from coucal.router import PeripheralInterface

@block
def byte_ram_en_ctrl(rw_enable, w_strobe, r_enable, w_enable, byte_offset):
    ''' This block sets the r_enable and w_enable signals for a particular
    byte.
    '''

    # Sanity checks
    assert(byte_offset < len(w_strobe))

    return_objects = []

    @always_comb
    def control():

        # Default the read and write enable signals low
        r_enable.next = False
        w_enable.next = False

        if rw_enable:
            if w_strobe == 0:
                # w_strobe is low so it must be a read
                r_enable.next = True

            else:
                # w_strobe is not zero so write enable depends on w_strobe.
                w_enable.next = w_strobe[byte_offset]

    return_objects.append(control)

    return return_objects

@block
def byte_ram(
    clock, r_enable, w_enable, r_data, w_data, mem_addr, memory_depth):
    ''' An 8 bit random access memory.

    Note: This block should not be created directly. It should be created via
    the ram block.
    '''

    # Sanity checks
    assert(len(r_data) == len(w_data))
    assert(len(r_data) == 8)

    if isinstance(mem_addr, int):
        # If the mem_addr is an int then it should be set to zero. The only
        # time this is acceptable is if the memory depth is one
        assert(memory_depth == 1)
        assert(mem_addr == 0)

    else:
        # Otherwise the mem_addr should be the correct width for the mem depth
        assert(2**len(mem_addr) == memory_depth)

    return_objects = []

    # Create the memory
    memory = [Signal(intbv(0)[8:]) for i in range(memory_depth)]

    @always(clock.posedge)
    def read_write():

        if r_enable:
            # Return the data
            r_data.next = memory[mem_addr]

        elif w_enable:
            # Write the data
            memory[mem_addr].next = w_data

    return_objects.append(read_write)

    return return_objects

@block
def ram(clock, ram_interface):
    ''' A random access memory. This block is designed to work with the
    router. The router will drive the `ram_interface.rw_enable` signal
    correctly as determined by the routing bits of the `ram_interface.rw_addr`
    signal. If this block is not used with the router then it may not function
    correctly as it only checks enough bits of the `ram_interface.rw_addr` to
    get the offset within the RAM.
    '''

    if not isinstance(ram_interface, PeripheralInterface):
        raise TypeError(
            'RAM: The ram_interface should be an instance of '
            'PeripheralInterface.')

    ram_n_bytes = ram_interface.n_bytes

    # Extract the signals from the ram_interface
    rw_enable = ram_interface.rw_enable
    rw_complete = ram_interface.rw_complete
    rw_addr = ram_interface.rw_addr
    w_strobe = ram_interface.w_strobe
    r_data = ram_interface.r_data
    w_data = ram_interface.w_data

    # Get the data bitwidth and byte width
    data_bitwidth = ram_interface.data_bitwidth
    data_bytewidth = ram_interface.data_bytewidth

    # Sanity checks: These should be checked in the PeripheralsInterface which
    # generates the ram_interface but we include sanity checks here to make
    # sure
    #
    # Check the read and write datas are the same width
    assert(len(r_data) == len(w_data))
    # Check data width is a multiple of 8
    assert(data_bitwidth % 8 == 0)
    # Check n_bytes is a multiple of the data byte width
    assert(ram_n_bytes % data_bytewidth == 0)
    # Check n_bytes is a power of 2
    assert(ram_n_bytes & (ram_n_bytes-1) == 0)
    # Check w_strobe is the correct width (equal to the data byte width)
    assert(len(w_strobe) == data_bytewidth)

    return_objects = []

    # Calculate the memory depth in words which are data_bytewidth
    memory_depth = ram_n_bytes//data_bytewidth

    if memory_depth == 1:
        # We have a single memory location so we can only ever read from or
        # write to that address
        mem_addr = 0
        # rw_addr is unused in this scenario. Set read on rw_addr to True to
        # suppress the conversion warning that signal is driven but not read.
        rw_addr.read = True

    else:
        # Calculate the lower index to convert from byte address to word
        # address
        mem_addr_lower_index = int(log(data_bytewidth)/log(2))

        # Extract the memory address from the rw_addr
        mem_addr = (
            rw_addr(
                ram_interface.n_memory_addressing_bits,
                mem_addr_lower_index))

    byte_r_datas = [Signal(intbv(0)[8:]) for n in range(data_bytewidth)]

    for n in range(data_bytewidth):

        # Create read and write enable signals for this byte_ram
        byte_r_enable = Signal(False)
        byte_w_enable = Signal(False)

        # Extract the relevant byte read data
        byte_r_data = byte_r_datas[n]

        # Slice the relevant byte out of the write data
        byte_w_data = w_data(8*(n+1), 8*n)

        # Create a block to control the byte read and write enable signals
        return_objects.append(
            byte_ram_en_ctrl(
                rw_enable, w_strobe, byte_r_enable, byte_w_enable, n))

        # Create an 8 bit wide RAM
        return_objects.append(
            byte_ram(
                clock, byte_r_enable, byte_w_enable, byte_r_data, byte_w_data,
                mem_addr, memory_depth))

    if data_bytewidth == 1:
        # Only one byte_r_data
        combined_byte_r_datas = byte_r_datas[0]

    else:
        # Combine the byte_r_datas onto a single signal
        combined_byte_r_datas = ConcatSignal(*reversed(byte_r_datas))

    assert(len(combined_byte_r_datas) == len(r_data))

    # Drive the r_data with the combined_byte_r_datas
    return_objects.append(signal_assigner(combined_byte_r_datas, r_data))

    t_state = enum('AWAIT_RW', 'PROPAGATION')
    state = Signal(t_state.AWAIT_RW)

    @always(clock.posedge)
    def control():

        if state == t_state.AWAIT_RW:
            if rw_enable:
                # Pulse the rw_complete
                rw_complete.next = True
                state.next = t_state.PROPAGATION

        elif state == t_state.PROPAGATION:
            # Give the core a cycle to receive the data and move on
            rw_complete.next = False
            state.next = t_state.AWAIT_RW

    return_objects.append(control)

    return return_objects
