import sys

from myhdl import Signal

# Check that this code is being run under python 3.7 or later. This is to make
# sure dictionaries are ordered.
assert(sys.version_info >= (3, 7))

AVAILABLE_IO_TYPES = ['rw', 'ro', 'wo']

class IOInterface(object):

    def __init__(self, io_signal_definitions):
        ''' An interface of IO signals.

        The io_signal_definitions argument should be a list of dictionaries
        in the form:

            [{'type': register 0 type,
              'signals': [
                  signal 0 name,
                  signal 1 name]},
             {'type': register 1 type,
              'signals': [
                  signal 2 name,
                  signal 3 name]}]

        The register types must be one of rw, ro, wo.

        All signal names must be unique.
        '''

        if len(io_signal_definitions) < 1:
            raise ValueError(
                'IOInterface: io_signal_definitions should contain at least '
                'one register.')

        self._n_registers = len(io_signal_definitions)

        for n in range(self._n_registers):
            if io_signal_definitions[n]['type'] not in AVAILABLE_IO_TYPES:
                raise ValueError(
                    'IOInterface: Register type should be one of ' +
                    ', '.join(AVAILABLE_IO_TYPES) + '. Requested type is '
                    + io_signal_definitions[n]['type'] + '.')

            if len(io_signal_definitions[n]['signals']) <= 0:
                raise ValueError(
                    'IOInterface: All registers should have at least one '
                    'signal.')

        io_names = []
        for n in range(self._n_registers):
            io_names.extend(io_signal_definitions[n]['signals'])

        # Check that the IO names are unique
        if len(io_names) != len(set(io_names)):
            raise ValueError(
                'IOInterface: IO signal names should be unique.')

        self._io_signal_definitions = io_signal_definitions

        for io_name in io_names:
            # Create the IO signal
            setattr(self, io_name, Signal(False))

    @property
    def n_registers(self):
        ''' Returns the number registers in the signal definitions.
        '''
        return self._n_registers

    def register_type(self, register_n):
        ''' Returns the type of register n.
        '''
        return self._io_signal_definitions[register_n]['type']

    def register_signals(self, register_n):
        ''' Returns a list containing all of the signals on register n.
        '''
        # Create a list containing all of the signals on the specified
        # register
        register_signals = [
            getattr(self, io_name) for io_name in
            self._io_signal_definitions[register_n]['signals']]

        return register_signals
