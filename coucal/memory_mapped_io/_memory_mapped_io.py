from math import log, ceil
from myhdl import (
    block, Signal, always_comb, intbv, always, ConcatSignal, enum)

from kea.utils import (
    constant_assigner, signal_assigner, signal_slicer,
    combined_signal_assigner)

from coucal.router import PeripheralInterface

from .interfaces import IOInterface

@block
def byte_rw_en_ctrl(rw_enable, w_strobe, w_enable, byte_offset):
    ''' This block sets the w_enable signals for a particular byte.
    '''

    # Sanity checks
    assert(byte_offset < len(w_strobe))

    return_objects = []

    @always_comb
    def control():

        # Default the write enable signal low
        w_enable.next = False

        if rw_enable:
            if w_strobe != 0:
                # w_strobe is not zero so write enable depends on w_strobe.
                w_enable.next = w_strobe[byte_offset]

    return_objects.append(control)

    return return_objects

@block
def byte_rw_reg_writer(
    clock, register_byte, byte_w_enable, w_data, mem_addr, io_signals,
    register_address):
    ''' An 8 bit read-write register writer.

    Note: This block should not be created directly. It should be created via
    the memory_mapped_io block.
    '''

    # Sanity checks
    assert(len(register_byte) == 8)
    assert(len(w_data) == 8)
    assert(len(io_signals) <= 8)
    assert(len(io_signals) > 0)

    if isinstance(mem_addr, int):
        # If the mem_addr is an int then it should be set to zero. The only
        # time this is acceptable is if the register address is 0
        assert(register_address == 0)
        assert(mem_addr == 0)

    else:
        # Otherwise the register address should be accessible by mem_addr
        assert(register_address < 2**len(mem_addr))

    return_objects = []

    n_io_signals = len(io_signals)

    for n in range(n_io_signals):
        # Connect the io signals to the relevant bit in the register_byte
        return_objects.append(
            signal_assigner(register_byte(n), io_signals[n]))

    @always(clock.posedge)
    def writer():

        if mem_addr == register_address and byte_w_enable:
            # Update the reg and io_signals with the relevant bits of the
            # w_data
            register_byte.next[n_io_signals:] = w_data[n_io_signals:]

    return_objects.append(writer)

    return return_objects

@block
def byte_wo_reg_writer(
    clock, register_byte, byte_w_enable, w_data, mem_addr, io_signals,
    register_address):
    ''' An 8 bit write only register writer.

    Note: This block should not be created directly. It should be created via
    the memory_mapped_io block.
    '''

    # Sanity checks
    assert(len(register_byte) == 8)
    assert(len(w_data) == 8)
    assert(len(io_signals) <= 8)
    assert(len(io_signals) > 0)

    if isinstance(mem_addr, int):
        # If the mem_addr is an int then it should be set to zero. The only
        # time this is acceptable is if the register address is 0
        assert(register_address == 0)
        assert(mem_addr == 0)

    else:
        # Otherwise the register address should be accessible by mem_addr
        assert(register_address < 2**len(mem_addr))

    return_objects = []

    n_io_signals = len(io_signals)
    io_interim = Signal(intbv(0)[n_io_signals:])

    for n in range(n_io_signals):
        # Connect the io signals to the relevant bit in the register_byte
        return_objects.append(signal_assigner(io_interim(n), io_signals[n]))

    t_state = enum('AWAIT_WRITE', 'PROPAGATION')
    state = Signal(t_state.AWAIT_WRITE)

    @always(clock.posedge)
    def writer():

        # The WO registers should always read as 0
        register_byte.next = 0

        if state == t_state.AWAIT_WRITE:
            if mem_addr == register_address and byte_w_enable:
                # Update the io_interim with the relevant bits of the w_data
                io_interim.next = w_data[n_io_signals:]
                state.next = t_state.PROPAGATION

        elif state == t_state.PROPAGATION:
            # The io_signals should only ever pulse the value
            io_interim.next = 0
            state.next = t_state.AWAIT_WRITE

    return_objects.append(writer)

    return return_objects

@block
def byte_ro_reg_writer(clock, register_byte, io_signals):
    ''' A 8 bit read only register writer.

    Note: This block should not be created directly. It should be created via
    the memory_mapped_io block.
    '''

    # Sanity checks
    assert(len(register_byte) == 8)
    assert(len(io_signals) <= 8)
    assert(len(io_signals) > 0)

    return_objects = []

    n_io_signals = len(io_signals)

    if n_io_signals == 1:
        # Only one io signal
        combined_io_signals = io_signals[0]

    else:
        # Combine the io_signals onto a single signal
        combined_io_signals = ConcatSignal(*reversed(io_signals))

    @always(clock.posedge)
    def writer():

        # Drive the relevant bits of the register with the io_signals
        register_byte.next[n_io_signals:] = combined_io_signals

    return_objects.append(writer)

    return return_objects

@block
def memory_mapped_io(clock, mem_interface, io_interface):
    ''' This block contains addressable IO signals which can be written to
    and/or read from. The io_interface contains IO signals which are grouped
    onto registers. Each register is individually addressable. Each register
    can be one of the following types:

        RW: Read-write - The core and read from and write to this IO
        RO: Read only  - The core can read this IO
        WO: Write only - The core can write to this IO causing a 1 cycle pulse

    Note: for a RW register all bits are updated by writes. To update a single
    bit the calling code should read the register, update any bits and then
    write the updated value.

    This block is designed to work with the router. The router will drive the
    `mem_interface.rw_enable` signal correctly as determined by the routing
    bits of the `mem_interface.rw_addr` signal. If this block is not used with
    the router then it may not function correctly as it only checks enough
    bits of the `mem_interface.rw_addr` to get the offset within the register
    address space.
    '''

    if not isinstance(mem_interface, PeripheralInterface):
        raise TypeError(
            'memory_mapped_io: The mem_interface should be an instance of '
            'PeripheralInterface.')

    if not isinstance(io_interface, IOInterface):
        raise TypeError(
            'memory_mapped_io: The io_interface should be an instance of '
            'IOInterface.')

    n_bytes = mem_interface.n_bytes

    # Extract the signals from the mem_interface
    rw_enable = mem_interface.rw_enable
    rw_complete = mem_interface.rw_complete
    rw_addr = mem_interface.rw_addr
    w_strobe = mem_interface.w_strobe
    r_data = mem_interface.r_data
    w_data = mem_interface.w_data

    # Get the data bitwidth and byte width
    data_bitwidth = mem_interface.data_bitwidth
    data_bytewidth = mem_interface.data_bytewidth

    # Calculate the memory depth in words which are data_bytewidth
    memory_depth = n_bytes//data_bytewidth

    n_registers = io_interface.n_registers

    # Create a list of the register types required
    register_types_required = []
    for n in range(n_registers):
        # Extract all the required register types
        register_type = io_interface.register_type(n)
        if io_interface.register_type(n) not in register_types_required:
            register_types_required.append(register_type)

    # Sanity checks: These should be checked in the PeripheralsInterface which
    # generates the mem_interface but we include sanity checks here to make
    # sure
    #
    # Check the read and write datas are the same width
    assert(len(r_data) == len(w_data))
    # Check data width is a multiple of 8
    assert(data_bitwidth % 8 == 0)
    # Check n_bytes is a multiple of the data byte width
    assert(n_bytes % data_bytewidth == 0)
    # Check n_bytes is a power of 2
    assert(n_bytes & (n_bytes-1) == 0)
    # Check w_strobe is the correct width (equal to the data byte width)
    assert(len(w_strobe) == data_bytewidth)
    # Check that the address space is large enough for all the register
    assert(n_registers <= memory_depth)
    # Check that all registers are of an expected type
    for register_type in register_types_required:
        assert(register_type in ['rw', 'ro', 'wo'])

    return_objects = []

    if register_types_required == ['ro']:
        # Only ro registers so w_data is unused. Set read on w_data to
        # suppress the conversion warning that the signal is driven but not
        # read.
        w_data.read = True

    if memory_depth == 1:
        # We have a single memory location
        mem_addr = 0
        # rw_addr is unused in this scenario. Set read on rw_addr to True to
        # suppress the conversion warning that signal is driven but not read.
        rw_addr.read = True

    else:
        # Calculate the lower index to convert from byte address to word
        # address
        mem_addr_lower_index = int(log(data_bytewidth)/log(2))

        # Extract the memory address from the rw_addr
        mem_addr = (
            rw_addr(
                mem_interface.n_memory_addressing_bits,
                mem_addr_lower_index))

    n_written_bytes = 0
    for n in range(n_registers):
        reg_type = io_interface.register_type(n)

        if reg_type == 'rw' or reg_type == 'wo':
            # Only rw and wo registers will use w_data to write to the
            # register. Check all the registers and see how much of the w_data
            # signal is actually used.
            n_register_signals = len(io_interface.register_signals(n))
            n_written_bytes_required = ceil(n_register_signals/8)

            if n_written_bytes_required > n_written_bytes:
                # If this register requires more bytes to be written than
                # currently accounted for, update the n_written_bytes to
                # reflect the required number
                n_written_bytes = n_written_bytes_required

    # Create byte write enables and write data bytes
    byte_w_enables = [Signal(False) for n in range(n_written_bytes)]
    w_data_bytes = [Signal(intbv(0)[8:]) for n in range(n_written_bytes)]

    for n in range(n_written_bytes):
        # Create the byte enable control block
        return_objects.append(
            byte_rw_en_ctrl(rw_enable, w_strobe, byte_w_enables[n], n))

        # Slice the write data and drive the w_data_bytes
        return_objects.append(signal_slicer(w_data, 8*n, 8, w_data_bytes[n]))

    registers = [Signal(intbv(0)[data_bitwidth:]) for n in range(n_registers)]

    for n in range(n_registers):
        # Extract the type and io signals for this register
        register_type = io_interface.register_type(n)
        register_io_signals = io_interface.register_signals(n)

        # Separate the io signals into chunks of up to 8
        register_io_chunks = [
            register_io_signals[n:n+8]
            for n in range(0, len(register_io_signals), 8)]

        # Create enough one byte registers for the byte width of the data
        register_bytes = [
            Signal(intbv(0)[8:]) for n in range(data_bytewidth)]

        for m in range(data_bytewidth):
            if m < len(register_io_chunks):
                # Extract the io signals for this byte
                register_io_chunk = register_io_chunks[m]

                if register_type == 'rw':
                    # Create the rw writer for this byte
                    return_objects.append(
                        byte_rw_reg_writer(
                            clock, register_bytes[m], byte_w_enables[m],
                            w_data_bytes[m], mem_addr, register_io_chunk, n))

                elif register_type == 'wo':
                    # Create the wo writer for this byte
                    return_objects.append(
                        byte_wo_reg_writer(
                            clock, register_bytes[m], byte_w_enables[m],
                            w_data_bytes[m], mem_addr, register_io_chunk, n))

                elif register_type == 'ro':
                    # Create the ro writer for this byte
                    return_objects.append(
                        byte_ro_reg_writer(
                            clock, register_bytes[m], register_io_chunk))

            else:
                # Drive all unused bytes with a 0
                return_objects.append(constant_assigner(0, register_bytes[m]))

        # Drive the register with the register_bytes
        return_objects.append(
            combined_signal_assigner(register_bytes, registers[n]))

    t_state = enum('AWAIT_READ', 'PROPAGATION')
    state = Signal(t_state.AWAIT_READ)

    @always(clock.posedge)
    def control():

        if state == t_state.AWAIT_READ:
            if rw_enable:
                # Pulse the rw_complete
                rw_complete.next = True

                if w_strobe == 0:
                    if mem_addr < n_registers:
                        # Output the data from the specified mem_addr. Placing
                        # the read here means the tools are won't infer a
                        # block RAM. This is OK as this block is GPIO so we
                        # don't want a block RAM anyway.
                        r_data.next = registers[mem_addr]

                    else:
                        r_data.next = 0

                state.next = t_state.PROPAGATION

        elif state == t_state.PROPAGATION:
            # Give the core a cycle to receive the data and move on
            rw_complete.next = False
            state.next = t_state.AWAIT_READ

    return_objects.append(control)

    return return_objects
