import random

from myhdl import Signal, block, always, enum, intbv, StopSimulation

from kea.test_utils import KeaTestCase as TestCase
from kea.test_utils import KeaVivadoVHDLTestCase as VivadoVHDLTestCase
from kea.test_utils import KeaVivadoVerilogTestCase as VivadoVerilogTestCase
from kea.axi import AxiLiteInterface
from kea.utils import and_gate

from coucal.router import (
    PeripheralsInterface, PeripheralInterface, random_address_map_generator)

from ._axi_lite_adapter import axi_lite_adapter

@block
def axi_lite_slave_responder(clock, axil_interface):
    ''' A simple responder which acts like an axi lite slave.
    '''

    assert(isinstance(axil_interface, AxiLiteInterface))

    return_objects = []

    w_addr_transaction = Signal(False)
    w_data_transaction = Signal(False)
    r_addr_transaction = Signal(False)

    w_addr_transaction_rxd = Signal(False)
    w_data_transaction_rxd = Signal(False)
    r_addr_transaction_rxd = Signal(False)

    return_objects.append(
        and_gate(
            axil_interface.AWVALID, axil_interface.AWREADY,
            w_addr_transaction))
    return_objects.append(
        and_gate(
            axil_interface.WVALID, axil_interface.WREADY,
            w_data_transaction))
    return_objects.append(
        and_gate(
            axil_interface.ARVALID, axil_interface.ARREADY,
            r_addr_transaction))

    @always(clock.posedge)
    def responder():

        # Write address
        # =============
        if w_addr_transaction:
            # Write address transaction has completed
            w_addr_transaction_rxd.next = True
            axil_interface.AWREADY.next = False

        if random.random() < 0.1:
            # Randomly set write address ready
            axil_interface.AWREADY.next = True

        # Write data
        # ==========
        if w_data_transaction:
            # Write data transaction has completed
            w_data_transaction_rxd.next = True
            axil_interface.WREADY.next = False

        if random.random() < 0.1:
            # Randomly set write data ready
            axil_interface.WREADY.next = True

        # Write response
        # ==============
        if ((w_addr_transaction or w_addr_transaction_rxd) and
            (w_data_transaction or w_data_transaction_rxd)):
            # Both write address and write data transactions have completed so
            # we need to send the write response

            if random.random() < 0.1:
                # Randomly send the write response
                axil_interface.BVALID.next = True
                axil_interface.BRESP.next = (
                    random.randrange(2**len(axil_interface.BRESP)))

                # The write address and write data transactions have been
                # responded to
                w_addr_transaction_rxd.next = False
                w_data_transaction_rxd.next = False

        if axil_interface.BVALID and axil_interface.BREADY:
            # The other end has received the response
            axil_interface.BVALID.next = False

        # Read address
        # ============
        if r_addr_transaction:
            # Read address transaction has completed
            r_addr_transaction_rxd.next = True
            axil_interface.ARREADY.next = False

        if random.random() < 0.1:
            # Randomly set read address ready
            axil_interface.ARREADY.next = True

        # Read response
        # ==============
        if r_addr_transaction or r_addr_transaction_rxd:
            # Read address transaction has completed so we need to send the
            # read response

            if random.random() < 0.1:
                # Randomly send the read response
                axil_interface.RVALID.next = True
                axil_interface.RDATA.next = (
                    random.randrange(2**len(axil_interface.RDATA)))
                axil_interface.RRESP.next = (
                    random.randrange(2**len(axil_interface.RRESP)))

                # The read address transaction has been responded to
                r_addr_transaction_rxd.next = False

        if axil_interface.RVALID and axil_interface.RREADY:
            # The other end has received the response
            axil_interface.RVALID.next = False

    return_objects.append(responder)

    return return_objects

def generate_mem_interface(offset=None, n_bytes=None):
    ''' Generate the mem_interface.
    '''

    n_peripherals = 1

    # Set an upper bound on the n_bytes so the tests don't take too long to
    # run
    n_bytes_upper_bound = 2**8 + 1

    zero_one_offset = bool(random.randrange(2))

    # Create a random address map
    address_map = (
        random_address_map_generator(
            n_peripherals, n_bytes_upper_bound, zero_one_offset))

    # The address should contain a single peripheral as we only need one for
    # peripheral interface for these tests.
    assert(len(address_map) == 1)

    if offset is not None:
        # Update the address map with the specified offset
        peripheral_name = list(address_map.keys())[0]
        address_map[peripheral_name]['offset'] = offset

    if n_bytes is not None:
        # Update the address map with the specified n_bytes
        peripheral_name = list(address_map.keys())[0]
        address_map[peripheral_name]['n_bytes'] = n_bytes

    # Create the peripherals interface
    peripherals_interface = PeripheralsInterface(address_map)

    peripheral_name = peripherals_interface.peripherals[0]
    mem_interface = (
        peripherals_interface.peripheral_interface_packager(peripheral_name))

    return mem_interface

def generate_axil_interface_and_types(
    data_bitwidth, addr_bitwidth, use_AWPROT=False, use_ARPROT=False,
    use_WSTRB=True):
    ''' Generate the axil_interface and types.
    '''

    axil_interface = (
        AxiLiteInterface(
            data_bitwidth, addr_bitwidth, use_AWPROT=use_AWPROT,
            use_ARPROT=use_ARPROT, use_WSTRB=use_WSTRB))

    axil_interface_types = {
        'AWVALID': 'output',
        'AWREADY': 'custom',
        'AWADDR': 'output',
        'WVALID': 'output',
        'WREADY': 'custom',
        'WDATA': 'output',
        'BVALID': 'custom',
        'BREADY': 'output',
        'BRESP': 'custom',
        'ARVALID': 'output',
        'ARREADY': 'custom',
        'ARADDR': 'output',
        'RVALID': 'custom',
        'RREADY': 'output',
        'RDATA': 'custom',
        'RRESP': 'custom',
    }

    if use_AWPROT:
        axil_interface_types['AWPROT'] = 'output'

    if use_ARPROT:
        axil_interface_types['ARPROT'] = 'output'

    if use_WSTRB:
        axil_interface_types['WSTRB'] = 'output'

    return axil_interface, axil_interface_types

def test_args_setup():
    ''' Generate the arguments and argument types for the DUT.
    '''

    clock = Signal(False)

    mem_interface = generate_mem_interface()

    # Get the data bitwidth
    data_bitwidth = mem_interface.data_bitwidth
    addr_bitwidth = len(mem_interface.rw_addr)

    axil_interface, axil_interface_types = (
        generate_axil_interface_and_types(data_bitwidth, addr_bitwidth))

    args = {
        'clock': clock,
        'mem_interface': mem_interface,
        'axil_interface': axil_interface,
    }

    mem_interface_types = {
        'rw_enable': 'custom',
        'rw_complete': 'output',
        'rw_addr': 'custom',
        'w_strobe': 'custom',
        'w_data': 'custom',
        'r_data': 'output',
    }

    arg_types = {
        'clock': 'clock',
        'mem_interface': mem_interface_types,
        'axil_interface': axil_interface_types,
    }

    return args, arg_types

class TestAxiLiteAdapterInterface(TestCase):
    ''' The DUT should reject incompatible interfaces and arguments.
    '''

    def setUp(self):

        self.args, _arg_types = test_args_setup()

    def test_invalid_mem_interface(self):
        ''' The `axi_lite_adapter` should raise an error if the
        `mem_interface` is not an instance of `PeripheralInterface`.
        '''

        self.args['mem_interface'] = random.randrange(0, 100)

        self.assertRaisesRegex(
            TypeError,
            ('axi_lite_adapter: The mem_interface should be an instance of '
             'PeripheralInterface.'),
            axi_lite_adapter,
            **self.args
        )

    def test_invalid_axil_interface(self):
        ''' The `axi_lite_adapter` should raise an error if the
        `axil_interface` is not an instance of `AxiLiteInterface`.
        '''

        self.args['axil_interface'] = random.randrange(0, 100)

        self.assertRaisesRegex(
            TypeError,
            ('axi_lite_adapter: The axil_interface should be an instance of '
             'AxiLiteInterface.'),
            axi_lite_adapter,
            **self.args
        )

    def test_axil_interface_invalid_addr_width(self):
        ''' The `axi_lite_adapter` should raise an error if the
        `axil_interface` address bitwidth is less than the `mem_interface`
        address bitwidth.
        '''

        data_bitwidth = self.args['mem_interface'].data_bitwidth

        invalid_bitwidth_upper_bound = len(self.args['mem_interface'].rw_addr)
        invalid_addr_bitwidth = (
            random.randrange(1, invalid_bitwidth_upper_bound))

        self.args['axil_interface'], _axil_interface_types = (
            generate_axil_interface_and_types(
                data_bitwidth, invalid_addr_bitwidth))

        self.assertRaisesRegex(
            ValueError,
            ('axi_lite_adapter: The axil_interface address should be at least '
             'as wide as the mem_interface address.'),
            axi_lite_adapter,
            **self.args
        )

    def test_mismatched_data_widths(self):
        ''' The `axi_lite_adapter` should raise an error if the
        `axil_interface` data bitwidth is not equal to the `mem_interface`
        data bitwidth.
        '''

        axil_data_bitwidth = 64
        addr_bitwidth = len(self.args['mem_interface'].rw_addr)

        self.args['axil_interface'], _axil_interface_types = (
            generate_axil_interface_and_types(
                axil_data_bitwidth, addr_bitwidth))

        self.assertRaisesRegex(
            ValueError,
            ('axi_lite_adapter: The mem_interface and the axil_interface '
             'should have the same data bitwidths.'),
            axi_lite_adapter,
            **self.args
        )

    def test_axil_interface_missing_w_strobe(self):
        ''' The `axi_lite_adapter` should raise an error if the
        `axil_interface` does not contain a `WSTRB` signal.
        '''

        data_bitwidth = self.args['mem_interface'].data_bitwidth
        addr_bitwidth = len(self.args['mem_interface'].rw_addr)

        self.args['axil_interface'], _axil_interface_types = (
            generate_axil_interface_and_types(
                data_bitwidth, addr_bitwidth, use_WSTRB=False))

        self.assertRaisesRegex(
            ValueError,
            ('axi_lite_adapter: The axil_interface should contain a write '
             'strobe.'),
            axi_lite_adapter,
            **self.args
        )

class TestAxiLiteAdapter(TestCase):

    def setUp(self):

        self.args, self.arg_types = test_args_setup()

        self.test_count = 0
        self.tests_run = False

    @block
    def end_tests(self, n_tests, **kwargs):

        clock = kwargs['clock']

        return_objects = []

        @always(clock.posedge)
        def control():

            if self.test_count >= n_tests:
                self.tests_run = True
                raise StopSimulation

        return_objects.append(control)

        return return_objects

    def random_stim_values(
        self, w_strobe_upper_bound, w_data_upper_bound,
        axil_offset, axil_addr_upper_bound, rw_addr_upper_bound):
        ''' This function generates stim values for the mem_interface inputs.
        '''

        axil_addr_space_max = axil_addr_upper_bound - 1
        rw_addr_max_val = rw_addr_upper_bound - 1

        w_data_val = random.randrange(w_data_upper_bound)

        if bool(random.randrange(2)):
            # Half the time set w_strobe to 0 so the DUT performs a read
            w_strobe_val = 0

        else:
            # Half the time set w_strobe to a non 0 value so the DUT
            # performs a write
            w_strobe_val = random.randrange(1, w_strobe_upper_bound)

        random_val = random.random()

        if random_val < 0.05:
            # Set rw_addr to 0 five per cent of the time
            rw_addr_val = 0

        elif random_val < 0.1:
            # Set rw_addr to its max value five per cent of the time
            rw_addr_val = rw_addr_max_val

        elif random_val < 0.15:
            # Set rw_addr to the AXI lite adapter offset five per cent of the
            # time
            rw_addr_val = axil_offset

        elif random_val < 0.2:
            # Set rw_addr to the highest address in the AXI lite adapter
            # address space five per cent of the time
            rw_addr_val = axil_addr_space_max

        elif random_val < 0.25 and (
            axil_addr_upper_bound < rw_addr_upper_bound):
            # If the next address after the DUT address space is a valid
            # address then set rw_addr to it five per cent of the time.
            rw_addr_val = axil_addr_upper_bound

        elif random_val < 0.5:
            # Randomly drive rw_addr twenty five per cent of the time
            rw_addr_val = random.randrange(rw_addr_upper_bound)

        else:
            # Select a random value in the DUT address space fifty per
            # cent of the time.
            rw_addr_val = (
                random.randrange(axil_offset, axil_addr_upper_bound))

        return w_strobe_val, w_data_val, rw_addr_val

    @block
    def mem_interface_stim(self, clock, mem_interface, hold_rw_enable_high):
        ''' This block randomly drives the mem_interface.
        '''

        assert(isinstance(mem_interface, PeripheralInterface))

        return_objects = []

        axil_offset = mem_interface.offset
        axil_n_bytes = mem_interface.n_bytes

        axil_addr_upper_bound = axil_offset + axil_n_bytes
        rw_addr_upper_bound = 2**len(mem_interface.rw_addr)

        w_strobe_upper_bound = 2**len(mem_interface.w_strobe)
        w_data_upper_bound = 2**len(mem_interface.w_data)

        t_state = enum('STIM', 'AWAIT_COMPLETE')
        state = Signal(t_state.STIM)

        @always(clock.posedge)
        def stim():

            if state == t_state.STIM:
                # Generate random stim values
                w_strobe_val, w_data_val, rw_addr_val = (
                    self.random_stim_values(
                        w_strobe_upper_bound, w_data_upper_bound,
                        axil_offset, axil_addr_upper_bound,
                        rw_addr_upper_bound))

                # Whilst rw_enable is low keep updating the other stim signals
                # with random values.
                mem_interface.rw_addr.next = rw_addr_val
                mem_interface.w_strobe.next = w_strobe_val
                mem_interface.w_data.next = w_data_val

                if random.random() < 0.2:
                    # Randomly set rw_enable
                    mem_interface.rw_enable.next = True
                    state.next = t_state.AWAIT_COMPLETE

            elif state == t_state.AWAIT_COMPLETE:
                if mem_interface.rw_complete:
                    # Generate random stim values
                    w_strobe_val, w_data_val, rw_addr_val = (
                        self.random_stim_values(
                            w_strobe_upper_bound, w_data_upper_bound,
                            axil_offset, axil_addr_upper_bound,
                            rw_addr_upper_bound))

                    mem_interface.rw_addr.next = rw_addr_val
                    mem_interface.w_strobe.next = w_strobe_val
                    mem_interface.w_data.next = w_data_val

                    if hold_rw_enable_high or random.random() < 0.1:
                        # Keep rw_enable high and set up another read
                        mem_interface.rw_enable.next = True

                    else:
                        mem_interface.rw_enable.next = False
                        state.next = t_state.STIM

        return_objects.append(stim)

        return return_objects

    @block
    def axi_lite_adapter_check(self, hold_rw_enable_high=False, **kwargs):

        clock = kwargs['clock']
        mem_interface = kwargs['mem_interface']
        axil_interface = kwargs['axil_interface']

        assert(isinstance(mem_interface, PeripheralInterface))
        assert(isinstance(axil_interface, AxiLiteInterface))

        return_objects = []

        return_objects.append(
            self.mem_interface_stim(
                clock, mem_interface, hold_rw_enable_high))

        return_objects.append(axi_lite_slave_responder(clock, axil_interface))

        expected_awvalid = Signal(False)
        expected_awaddr = Signal(intbv(0)[len(axil_interface.AWADDR):])
        expected_wvalid = Signal(False)
        expected_wdata = Signal(intbv(0)[len(axil_interface.WDATA):])
        expected_wstrb = Signal(intbv(0)[len(axil_interface.WSTRB):])
        expected_bready = Signal(False)
        expected_arvalid = Signal(False)
        expected_araddr = Signal(intbv(0)[len(axil_interface.ARADDR):])
        expected_rready = Signal(False)

        expected_rw_complete = Signal(False)
        expected_r_data = Signal(intbv(0)[len(mem_interface.r_data):])

        t_state = enum('IDLE', 'TRANSACTION')
        state = Signal(t_state.IDLE)

        if hasattr(axil_interface, 'AWPROT'):

            @always(clock.posedge)
            def check_awprot():
                assert(axil_interface.AWPROT == 0)

            return_objects.append(check_awprot)

        if hasattr(axil_interface, 'ARPROT'):

            @always(clock.posedge)
            def check_arprot():
                assert(axil_interface.ARPROT == 0)

            return_objects.append(check_arprot)

        @always(clock.posedge)
        def check():

            assert(axil_interface.AWVALID == expected_awvalid)
            assert(axil_interface.AWADDR == expected_awaddr)
            assert(axil_interface.WVALID == expected_wvalid)
            assert(axil_interface.WDATA == expected_wdata)
            assert(axil_interface.WSTRB == expected_wstrb)
            assert(axil_interface.BREADY == expected_bready)
            assert(axil_interface.ARVALID == expected_arvalid)
            assert(axil_interface.ARADDR == expected_araddr)
            assert(axil_interface.RREADY == expected_rready)

            assert(mem_interface.rw_complete == expected_rw_complete)
            assert(mem_interface.r_data == expected_r_data)

            # rw_complete should only pulse
            expected_rw_complete.next = False

            # Address, data and strobe should always be forwarded.
            expected_awaddr.next = mem_interface.rw_addr
            expected_araddr.next = mem_interface.rw_addr
            expected_wdata.next = mem_interface.w_data
            expected_wstrb.next = mem_interface.w_strobe

            expected_r_data.next = axil_interface.RDATA

            if state == t_state.IDLE:
                if mem_interface.rw_enable and not mem_interface.rw_complete:

                    state.next = t_state.TRANSACTION

                    if mem_interface.w_strobe != 0:

                        # Write transaction so DUT should set up a write
                        # address and write data. It should also set ready for
                        # the response
                        expected_awvalid.next = True
                        expected_wvalid.next = True
                        expected_bready.next = True

                    else:

                        # Read transaction so DUT should set up a read
                        # address. It should also set ready for the response
                        expected_arvalid.next = True
                        expected_rready.next = True

            elif state == t_state.TRANSACTION:

                # Write
                # =====
                if expected_awvalid and axil_interface.AWREADY:
                    # Write data has been received
                    expected_awvalid.next = False

                if expected_wvalid and axil_interface.WREADY:
                    # Write data has been received
                    expected_wvalid.next = False

                if axil_interface.BVALID and expected_bready:
                    # Response has been sent back so write has completed.
                    expected_bready.next = False
                    expected_rw_complete.next = True

                    state.next = t_state.IDLE

                # Read
                # ====
                if expected_arvalid and axil_interface.ARREADY:
                    # Read address has been received
                    expected_arvalid.next = False

                if axil_interface.RVALID and expected_rready:
                    # Response has been sent back so the read has completed
                    expected_rready.next = False
                    expected_rw_complete.next = True

                    state.next = t_state.IDLE

            if mem_interface.rw_enable and mem_interface.rw_complete:
                self.test_count += 1

        return_objects.append(check)

        return return_objects

    def test_adapter(self):
        ''' The `axi_lite_adapter` should convert the `mem_interface` from the
        core's memory protocol to the AXI lite protocol on the
        `axil_interface`.

        Please see the AXI lite protocol specification for the details on the
        AXI lite protocol.

        The `axi_lite_adapter` should wait for `mem_interface.rw_enable` to go
        high. If `mem_interface.w_strobe` is 0 then it should perform an AXI
        read otherwise it should perform an AXI write.

        Reads
        =====
        The DUT should set up an AXI read of the address on
        `mem_interface.rw_addr`. When the read is complete the DUT should
        return the data on `mem_interface.r_data` and set
        `mem_interface.rw_complete` for one cycle.

        Writes
        ======
        The DUT should set up an AXI write of the data on
        `mem_interface.w_data` to the address on `mem_interface.rw_addr`. When
        the AXI is complete the DUT should set `mem_interface.rw_complete` for
        one cycle.

        We rely on the control signals of the interfaces to get the timing
        right. For this reason the non controlling signals on the
        `mem_interface` and `axil_interface` should always be forwarded to
        each other:

            `mem_interface.rw_addr` => `axil_interface.AWADDR`
            `mem_interface.rw_addr` => `axil_interface.ARADDR`
            `mem_interface.w_data` => `axil_interface.WDATA`
            `mem_interface.w_strobe` => `axil_interface.WSTRB`

            `axil_interface.RDATA` => `mem_interface.r_data`
        '''

        cycles = 20000
        n_tests = 200

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.axi_lite_adapter_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, axi_lite_adapter, axi_lite_adapter, self.args,
            self.arg_types, custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_back_to_back_reads_and_writes(self):
        ''' The `axi_lite_adapter` should function correctly when back to back
        reads from and writes to different addresses are requested.
        '''

        cycles = 20000
        n_tests = 200

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(
                self.axi_lite_adapter_check(
                    hold_rw_enable_high=True, **kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, axi_lite_adapter, axi_lite_adapter, self.args,
            self.arg_types, custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

    def test_arprot_and_awprot(self):
        ''' The `axil_interface.ARPROT` and `axil_interface.AWPROT` should be
        driven with 0 if they are present on the `axil_interface`.
        '''

        cycles = 20000
        n_tests = 200

        data_bitwidth = self.args['mem_interface'].data_bitwidth
        addr_bitwidth = len(self.args['mem_interface'].rw_addr)

        self.args['axil_interface'], self.arg_types['axil_interface'] = (
            generate_axil_interface_and_types(
                data_bitwidth, addr_bitwidth, use_AWPROT=True,
                use_ARPROT=True, use_WSTRB=True))

        @block
        def stimulate_check(**kwargs):

            return_objects = []

            return_objects.append(self.end_tests(n_tests, **kwargs))

            return_objects.append(self.axi_lite_adapter_check(**kwargs))

            return return_objects

        dut_outputs, ref_outputs = self.cosimulate(
            cycles, axi_lite_adapter, axi_lite_adapter, self.args,
            self.arg_types, custom_sources=[(stimulate_check, (), self.args)])

        assert(self.tests_run)
        self.assertEqual(dut_outputs, ref_outputs)

class TestAxiLiteAdapterVivadoVhdl(
    VivadoVHDLTestCase, TestAxiLiteAdapter):
    pass

class TestAxiLiteAdapterVivadoVerilog(
    VivadoVerilogTestCase, TestAxiLiteAdapter):
    pass
