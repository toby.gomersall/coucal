from myhdl import Signal, intbv

CORE_MEMORY_INTERFACE_TYPES = {
    'rw_enable': 'custom',
    'rw_complete': 'output',
    'rw_addr': 'custom',
    'w_strobe': 'custom',
    'w_data': 'custom',
    'r_data': 'output',
}

ADDR_BITWIDTH = 32
DATA_BITWIDTH = 32
W_STROBE_BITWIDTH = 4
INTERRUPT_BITWIDTH = 32

class CoreMemoryInterface(object):

    def __init__(self):
        ''' The native memory interface of the core.
        '''

        self._data_bitwidth = DATA_BITWIDTH
        assert(self._data_bitwidth % 8 == 0)
        self._data_bytewidth = self._data_bitwidth//8

        self.rw_enable = Signal(False)
        self.rw_complete = Signal(False)
        self.rw_addr = Signal(intbv(0)[ADDR_BITWIDTH:0])
        self.w_strobe = Signal(intbv(0)[W_STROBE_BITWIDTH:0])
        self.w_data = Signal(intbv(0)[self._data_bitwidth:0])
        self.r_data = Signal(intbv(0)[self._data_bitwidth:0])

    @property
    def data_bitwidth(self):
        ''' Returns the bitwidth of the data signals on this interface.
        '''
        return self._data_bitwidth

    @property
    def data_bytewidth(self):
        ''' Returns the byte width of the data signals on this interface.
        '''
        return self._data_bytewidth
